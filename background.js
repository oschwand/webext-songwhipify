let baseURL = "https://songwhip.com";
let pattern = "https://open.spotify.com/intl-fr/track/*";

async function searchSongwhip(url) {
    return fetch(baseURL, {
        method: "POST",
        body: JSON.stringify({"url": url}),
    })
    .then(response => response.json())
    .then(data => baseURL + data.pagePath)
    .catch(error => {
        console.error("Songwhip API error:", error);
        throw new Error("Songwhip API error:", error)
    });
}

function redirect(requestDetails) {
    return searchSongwhip(requestDetails.url)
    .then(targetURL => {console.log(`Redirecting to ${targetURL}`); return {redirectUrl: targetURL}})
    .catch(error => {});
};

browser.webRequest.onBeforeRequest.addListener(
  redirect,
  { urls: [pattern], types: ["main_frame"] },
  ["blocking"],
);
